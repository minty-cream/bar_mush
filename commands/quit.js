module.exports = {
	"usage":"DISCONNECT # Disconnect\n",
	"verify":(_options, socket, knex)=>Promise.resolve({__res:true,__socket:socket, __knex:knex}),
	"modify":(_options, socket, knex)=>Promise.resolve({__res:true,__socket:socket, __knex:knex}),
	"action":(_options, socket, knex)=>{
		socket.end("Goodbye!\n");
		return Promise.resolve({__res:true,__socket:socket,__knex:knex});
	}
}
