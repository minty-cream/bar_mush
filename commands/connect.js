const bcrypt=require('bcryptjs')
const slugid=require('slugid')

module.exports = {
	"usage":"Usage: create <username> <password> # You may have spaces in your username, but not your password.\n",
	"verify":(_options, socket, knex)=>{
		console.log("_options",_options);
		let _split=_options.trim().lastIndexOf(" ");
		if(_split > -1){
			let _username=_options.substr(0,_split).trim();
			let _password=_options.substr(_split).trim();
			return knex('users')
				.where('username',_username)
				.select()
				.then((resp)=>new Promise((res,rej)=>{
					if(resp.length > 0){
						rej("Sorry, that username is already taken.");
					}else{
						res({
							__res:{
								__username:_username,
								__password:_password
							},
							__socket:socket,
							__knex:knex
						});
					}
				}));
		}else{
			return Promise.reject(module.exports.usage);
		}
	},
	"modify":(_options, socket, knex)=>{
		console.log("_options",_options);
		let _saltRounds=10;
		return bcrypt.hash(_options.__password,_saltRounds)
			.then((hash)=>knex('users')
				.insert({
					id:slugid.nice(),
					username:_options.__username,
					password:hash
				}))
			.then((resp)=>Promise.resolve({__res:true,__socket:socket,__knex:knex}));
	},
	"action":(_options, socket, knex)=>{ socket.write("Character created!\n") }
}
