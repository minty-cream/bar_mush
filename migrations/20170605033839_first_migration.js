exports.up = (knex, Promise)=>Promise.all([
	knex.schema.createTable('users',(t)=>{
		t.string('id',22).unique().primary().notNullable();
		t.string('username').unique().notNullable();
		t.text('password').notNullable();
		t.string('descriptions_id');
		t.timestamps(true,true);
	}),
	knex.schema.createTable('locations',(t)=>{
		/**
		 * locations include: apartments, and sandboxes
		 **/
		t.string('id',22).unique().primary().notNullable();
		t.string('title').notNullable();
		t.string('users_id').notNullable().references('users.id').onDelete('CASCADE');
		t.enu('type',['apartment','sandbox']);
		t.boolean('recordable');
		t.string('descriptions_id');
		t.timestamps(true,true);
	}),
	knex.schema.createTable('channels',(t)=>{
		t.string('id',22).unique().primary().notNullable();
		t.string('title').unique().notNullable();
		t.text('description').notNullable();
		t.timestamps(true,true);
	}),
	knex.schema.createTable('descriptions',(t)=>{
		t.string('id',22).unique().primary().notNullable();
		t.string('users_id').notNullable().references('users.id').onDelete('CASCADE');
		t.string('title').notNullable();
		t.text('description').notNullable();
		t.timestamps(true,true);
	}),
	knex.schema.createTable('messages',(t)=>{
		t.string('id',22).unique().primary().notNullable();
		t.string('users_id').notNullable().references('users.id').onDelete('NO ACTION');
		t.string('locations_id').notNullable();
		t.text('message').notNullable();
		t.timestamps(true,true);
	}),
	knex.schema.createTable('mails',(t)=>{
		t.string('id',22).unique().primary().notNullable();
		t.string('users_id').notNullable().references('users.id').onDelete('NO ACTION');
		t.string('sender_id').notNullable().references('users.id').onDelete('NO ACTION');
		t.text('message').notNullable();
		t.timestamps(true,true);
	}),
	knex.schema.createTable('kinks',(t)=>{
		t.string('id',22).unique().primary().notNullable();
		t.string('users_id').notNullable().references('users.id').onDelete('CASCADE');
		t.string('title').notNullable();
		t.text('note');
		t.timestamps(true,true);
	}),
	knex.schema.createTable('squicks',(t)=>{
		t.string('id',22).unique().primary().notNullable();
		t.string('users_id').notNullable().references('users.id').onDelete('CASCADE');
		t.string('title').notNullable();
		t.text('note');
		t.timestamps(true,true);
	}),
	knex.schema.createTable('ignores',(t)=>{
		t.string('id',22).unique().primary().notNullable();
		t.string('users_id').notNullable().references('users.id').onDelete('CASCADE');
		t.string('target_id').notNullable().references('users.id').onDelete('CASCADE');
		t.text('note');
		t.timestamps(true,true);
	}),
	knex.schema.createTable('watches',(t)=>{
		t.string('id',22).unique().primary().notNullable();
		t.string('users_id').notNullable().references('users.id').onDelete('CASCADE');
		t.string('target_id').notNullable().references('users.id').onDelete('CASCADE');
		t.text('note');
		t.timestamps(true,true);
	})
]);

exports.down = (knex, Promise)=>Promise.all([
	//Just delete the database manually
]);
