const net = require('net');
const _NODE_ENV = process.env.NODE_ENV || 'development'
const knex=require('knex')(require('./knexfile')[_NODE_ENV]);

let sockets = [];

/*
 * Cleans the input of carriage return, newline
 */
function cleanInput(data) {
	return data.toString().replace(/(\r\n|\n|\r)/gm,"").trim();
}

/*
 * Method executed when data is received from a socket
 */

const _unauthCommands={
	"create": require('./commands/create'),
	"connect": require('./commands/connect'),
	"DISCONNECT": require('./commands/quit')
};

const _commands={
	"DISCONNECT": require('./commands/quit'),
	"LOGOUT": require('./commands/logout'),
	"@DELETEME": require('./commands/deleteme'),
	"say": require('./commands/say'),
	"pose": require('./commands/pose'),
	"@sandbox": require('./commands/sandbox'),
	"@apartment": require('./commands/apartment'),
	"@mail": require('./commands/mail'),
	"@mail/read": require('./commands/mail_read'),
	"@summon": require('./commands/summon'),
	"@join": require('./commands/join')
};

//channel stuff needs to be its own thing, I'm certain.

function receiveData(socket, data) {
	let cleanData = cleanInput(data).split(/ (.+)/);
	let _command='';
	let __commands=_unauthCommands;
	if(sockets.id) __commands=_commands;
	let _options='';
	if(cleanData[0]) _command=cleanData[0].trim();
	if(cleanData[1]) _options=cleanData[1].trim();
	if(_command==="\""){
		_command="say";
	}
	if(_command===":"){
		_command="pose";
	}

	if(__commands[_command]){
		__commands[_command]
			.verify(_options, socket, knex)
			.then(({__res,__socket,__knex})=>__commands[_command].modify(__res,__socket,__knex))
			.then(({__res,__socket,__knex})=>__commands[_command].action(__res,__socket,__knex))
			.catch((err)=>{ socket.write(`${err}\n`); });
	} else {
		socket.write(`Sorry, that isn't a valid command.\n`); 
	}
}

/*
 * Method executed when a socket ends
 */
function closeSocket(socket) {
	var i = sockets.indexOf(socket);
	if (i != -1) {
		sockets.splice(i, 1);
	}
}

/*
 * Callback method executed when a new TCP socket is opened.
 */
function newSocket(socket) {
	sockets.push(socket);
	socket.write(`Welcome to Bar Mush!\n${_unauthCommands.create.usage}${_unauthCommands.connect.usage}${_unauthCommands.DISCONNECT.usage}`);
	socket.on('data', function(data) {
		receiveData(socket, data);
	})
	socket.on('end', function() {
		closeSocket(socket);
	})
}

// Create a new server and provide a callback for when a connection occurs
var server = net.createServer(newSocket);

// Listen on port 8888
knex.migrate.latest()
	.then(()=>{
		server.listen(3000);
		console.log("Started");
	});
